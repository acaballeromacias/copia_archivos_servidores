#!/bin/sh
#===========================================================================
# Proyecto:  22094 - Implementación de conexión hacia el servidor de Windows
# Autor   :  Willy Mateo Espinoza
# Lider PDS: SUD Monica Arce
# Lider SIS: TIC
#===========================================================================

# variables de entorno
ORACLE_BASE=/oracle/app/oracle
ORACLE_HOME="$ORACLE_BASE/product/12.2.0/dbhome_1"
PATH="$ORACLE_HOME/bin:/bin:/usr/bin:/usr/local/bin:/usr/bin/X11:/opt/openssl/bin:/usr/lib/hpux32"

export ORACLE_SID ORACLE_BASE ORACLE_HOME
export PATH=/oracle/app/oracle/product/12.2.0/dbhome_1/bin:/bin:/usr/bin:/usr/local/bin:/opt/java1.5/bin:/opt/java6/jre/bin:/opt/java11/bin/

# RutaTemporal=`cat $Ruta/$ArchivoConfiguracion | grep -w "RUTATEMPORAL" | awk -F\= '{print $2}'`
# javaPath=`cat $Ruta/$ArchivoConfiguracion | grep -w "JAVAPATH" | awk -F\= '{print $2}'`
Ruta=/procesos/gsioper
ArchivoConfiguracion=".configFileSga"
RutaTemporal=/tmp
javaPath=/tmp/jdk1.5.0_22/bin

# Configuración DB
##############################################################
# hostDb=`cat $Ruta/$ArchivoConfiguracion | grep -w "HOSTDB" | awk -F\= '{print $2}'`
# portDb=`cat $Ruta/$ArchivoConfiguracion | grep -w "PORTDB" | awk -F\= '{print $2}'`
# usuarioDb=`cat $Ruta/$ArchivoConfiguracion | grep -w "USUARIODB" | awk -F\= '{print $2}'`
# passwordDb=`cat $Ruta/$ArchivoConfiguracion | grep -w "PASSWORDDB" | awk -F\= '{print $2}'`
# SID=`cat $Ruta/$ArchivoConfiguracion | grep -w "SIDDB" | awk -F\= '{print $2}'`
cadenaConexionDb=`cat $Ruta/$ArchivoConfiguracion | grep -w "CADENACONEXION" | awk -F\= '{print $2}'`

# Configuración remota
##############################################################
# hostRemoto=`cat $Ruta/$ArchivoConfiguracion | grep -w "HOSTREMOTO" | awk -F\= '{print $2}'`
# usuario=`cat $Ruta/$ArchivoConfiguracion | grep -w "USUARIOREMOTO" | awk -F\= '{print $2}'`
# pass=`cat $Ruta/$ArchivoConfiguracion | grep -w "PASSWORDREMOTO" | awk -F\= '{print $2}'`
# rutaRemota=`cat $Ruta/$ArchivoConfiguracion | grep -w "RUTAREMOTA" | awk -F\= '{print $2}'`
hostRemoto="10.243.15.23"
usuario="gsioper"
pass="c5ZQv@Yf"
rutaRemota="/tmp"

date > $Ruta/transferencia_bio.log
chmod 777 $Ruta/transferencia_bio.log
echo "Iniciando proceso de transferencia de archivos..." >> $Ruta/transferencia_bio.log
#==========================================================================================================================#
#1. Generar reporte, se genera en 10.243.0.55 PRCSIS/TEMPORAL
echo >> $Ruta/transferencia_bio.log
echo Usuario de BD: $usuarioDb >> $Ruta/transferencia_bio.log
echo SID: $SID >> $Ruta/transferencia_bio.log
echo Ejecutando proceso marketing.pck_reporte_biofeeder.prc_genera_archivo... >> $Ruta/transferencia_bio.log

output_reports_generated=`sqlplus -L -s $cadenaConexionDb <<EOF
SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF
        declare
                pn_error varchar2(500);
                pv_error varchar2(500);
        begin
                marketing.PCK_REPORTE_BIOFEEDER.prc_genera_archivo(pn_error, pv_error);
        end;
EXIT;
EOF`

echo Oracle output: $output_reports_generated >> $Ruta/transferencia_bio.log
#==========================================================================================================================#
#2. Obtener nombre de documentos
echo >> $Ruta/transferencia_bio.log
echo Obteniendo nombre de archivos a transferir... >> $Ruta/transferencia_bio.log
nom_archivo_bio=`sqlplus -L -s $cadenaConexionDb <<EOF
SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF
    select b.nombre_archivo from marketing.mkt_solicitud_biofeeder b where b.estado='E';
EXIT;
EOF`

echo Oracle output: $nom_archivo_bio >> $Ruta/transferencia_bio.log
if [ "$nom_archivo_bio" ]; then
documentos="${documentos},$nom_archivo_bio"
echo Nombres de archivos recuperados exitosamente >> $Ruta/transferencia_bio.log
else
echo Error al recuperar nombre de archivos >> $Ruta/transferencia_bio.log
exit 1
fi
#==========================================================================================================================#
#3. Descargar archivos
echo >> $Ruta/transferencia_bio.log
echo Descargando archivos... >> $Ruta/transferencia_bio.log
echo Usuario: $usuario >> $Ruta/transferencia_bio.log
echo Password: $pass >> $Ruta/transferencia_bio.log
echo Host remoto: $hostRemoto >> $Ruta/transferencia_bio.log
echo Ruta remota: $rutaRemota >> $Ruta/transferencia_bio.log

for archivo in $(echo $documentos | sed "s/,/ /g"); do
echo >> $Ruta/transferencia_bio.log
echo Descargando $RutaTemporal/$archivo... >> $Ruta/transferencia_bio.log
tranferencia_output=$($javaPath/java -jar $Ruta/sftp.jar "$hostRemoto" "$usuario" "$pass" D "$RutaTemporal" "$rutaRemota/$archivo" 2>&1) $tranferencia_output >> $Ruta/transferencia_bio.log
#Descarga fallida
if [ "$tranferencia_output" ]; then
echo Error al descargar $archivo >> $Ruta/transferencia_bio.log

#Transferido exitosamente
else
echo Descarga realizada exitosamente >> $Ruta/transferencia_bio.log
# Marcar como transferido
echo Actualizando estado de archivo... >> $Ruta/transferencia_bio.log
output_update=`sqlplus -L -s $cadenaConexionDb <<EOF
SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF
    update marketing.mkt_solicitud_biofeeder b set b.estado='T' where b.nombre_archivo = '$archivo';
EXIT;
EOF`
echo Oracle output: $output_update >> $Ruta/transferencia_bio.log
#Eliminar archivo de la ruta remota
echo Eliminando $archivo... >> $Ruta/transferencia_bio.log
salida_elimiacion=$($javaPath/java -jar $Ruta/sftp.jar "$hostRemoto" "$usuario" "$pass" DEL "$rutaRemota/$archivo" 2>&1)
echo Archivo "$hostRemoto:$rutaRemota/$archivo" elminiado correctamente>> $Ruta/transferencia_bio.log
fi

done
