
package claro;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Vector;

public class FtpsTls {
	
	public static void sftp(String pUser, String pPass, String pHost, int pPort, 
		String pOutputStream, String pPathFile, String ptipo) throws Exception {
    
	JSch sftp = new JSch();
    Session session = null;
    ChannelSftp channelSftp = null;
    
    Vector<Object> list = new Vector();
    ChannelSftp.LsEntry lsEntry = null;
    SftpATTRS attrs = null;
    
    try {
      
      session = sftp.getSession(pUser, pHost, pPort);
      session.setPassword(pPass);
      Properties prop = new Properties();
      prop.put("StrictHostKeyChecking", "no");
      session.setConfig(prop);
      session.connect();
      channelSftp = (ChannelSftp)session.openChannel("sftp");
      channelSftp.connect();
      
      if (ptipo.equals("L")) {
        
        list = channelSftp.ls(String.valueOf(pOutputStream));
        
        for (int i = 0; i < list.size(); i++) {
          System.out.println(list.get(i)); }
      
      } 
      
      if (ptipo.equals("D")) {
    	
    	String[] paths = pOutputStream.split("/");
        OutputStream os = new BufferedOutputStream(
        	new FileOutputStream( pPathFile + "/" + paths[paths.length - 1] ));
        channelSftp.get(String.valueOf(pOutputStream), os);
        
      }
      
      if (ptipo.equals("DEL")) {
        channelSftp.rm(String.valueOf(pOutputStream)); }
      
      if (ptipo.equals("P")) {
        
    	channelSftp.cd(pPathFile);
        File f = new File(pOutputStream);
        channelSftp.put(new FileInputStream(f), f.getName());
        
      }
    
    } catch (Exception e) {
      throw new Exception(e); }
    
    finally {
      
      if (channelSftp.isConnected()) {
        channelSftp.disconnect(); }
      
      if (session.isConnected()) {
        session.disconnect(); }
  
    } 
  
  }
  
  public static void main(String[] args) {
    
    String user = args[1];
    String tipoej = args[3];
    String password = args[2];
    String servidor = args[0];
    String dirdestino = null, archdestino = null, archlocal = null, tipo = null;
    
    if (tipoej.equals("P")) {
      
      dirdestino = args[4];
      archlocal = args[5];
    
    } 
    
    if (tipoej.equals("D")) {
      
      dirdestino = args[4];
      archdestino = args[5];
    
    } 
    
    if (tipoej.equals("DEL")) {
      archdestino = args[4]; } 
    
    if (tipoej.equals("L")) {
      archdestino = args[4]; } 
    
    try {
	  
    	if (tipoej.equals("P")){
    		archdestino = archlocal; }
    
    	sftp(user, password, servidor, 22, archdestino, dirdestino, tipoej);
    
    } catch (Exception e) {
    	e.printStackTrace(); }
    
  }

}
